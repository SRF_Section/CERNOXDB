#!/usr/bin/env python3
import os
import sys
import re
import shutil
import warnings
import argparse
import numpy as np
import pandas as pd
from openpyxl import load_workbook
from termcolor import colored as clr
import matplotlib.pyplot as plt
import CERNOXclasses
warnings.filterwarnings('ignore', category=UserWarning, module='openpyxl')
version={'maj':2,'min':4,'auth':'PP','when':'2025-02-10 [regex vs. inconsistencies]'}

def vstring() -> str:
    '''The Version string for the program'''
    pname=os.path.basename(sys.argv[0])
    prog=clr(f'{pname:s}', color='blue')
    comma=clr(',',color='black')
    ver=clr(f'V.{version['maj']:d}.{version['min']:d}', color='red')
    by=clr(' by ',color='black')
    auth=clr(f'{version['auth']:s}','blue')
    on=clr(' on ',color='black')
    when=clr(f'{version['when']:s}',color='light_red')
    return prog,comma,ver,comma,by,auth,on,when

def printSensorList(NITEMS, sensors: list) -> None:
    '''Lists on the screen the sensors ans the number'''
    print('Sensors id in the DB:')
    for i,s in enumerate(sensors,start=1):
        print(clr(s,color='magenta'),end='')
        if i%NITEMS:
            print(' ',end='')
        else:
            print('')
    print(f'\nFound {len(sensors):d} sensors.')

def colorize(nmaster: int, ndep: int) -> tuple:
    '''Sets the attributes for the number to print'''
    if ndep==nmaster:
        return 'green',['bold']
    else:
        return 'red',['bold','blink']

def convertFICAndSave(sensor: str) -> type[CERNOXclasses.CXTS]:
    '''Makes the conversion from FIC to HDF and saves the HDF'''
    c=CERNOXclasses.CXTS.Initialize(sensor)
    c.WriteHDFFile(CERNOXclasses.CXTS.dataPath)
    return c

def fitandaddChebyshev(sensor: str) -> type[CERNOXclasses.CXTS]:
    '''Updates the sensor, fit, store and save cof'''
    name=os.path.join(CERNOXclasses.CXTS.dataPath,'HDF',sensor+'.hdf')
    c=CERNOXclasses.CXTS.LoadHDFfile(name)
    c.fitChebyshev()
    c.storeChebyshev(CERNOXclasses.CXTS.dataPath)
    c.saveChebyshev(CERNOXclasses.CXTS.dataPath)
    return c

def loadHDFsensor(name: str) -> type[CERNOXclasses.CXTS]:
    '''Returns the sensor structure from HDF from name'''
    fname=os.path.join(CERNOXclasses.CXTS.dataPath,'HDF',name+'.hdf')
    c=CERNOXclasses.CXTS.LoadHDFfile(fname)
    return c

def showDBStatus():
    sensorlist=sorted(CERNOXclasses.CXTS.getSensorList())
    hdflist=sorted(CERNOXclasses.CXTS.getHDFList())
    coflist=sorted(CERNOXclasses.CXTS.getCOFList())
    f34list=sorted(CERNOXclasses.CXTS.get340List())
    nsens=len(sensorlist)
    nhdf=len(hdflist)
    ncof=len(coflist)
    nf34=len(f34list)
    print('The database contains:')
    print(clr(f'{nsens:3d}',color='green',attrs=['bold']),end='')
    print(' sensors described by FIC files (2 each)')
    col, att = colorize(nsens, nhdf)
    print(clr(f'{nhdf:3d}',color=col,attrs=att),end='')
    print(' normalized HDF files')
    col, att = colorize(nsens, ncof)
    print(clr(f'{ncof:3d}',color=col,attrs=att),end='')
    print(' chebychef coefficient files')
    col, att = colorize(nsens, nf34)
    print(clr(f'{nf34:3d}',color=col,attrs=att),end='')
    print(' Lakeshore 340 files')
    return sensorlist,nsens,hdflist,nhdf,coflist,ncof,f34list,nf34

if __name__ == '__main__':
    WID,HEI=os.get_terminal_size()
    NITEMS=WID//14
    CERNOXclasses.CXTS.dataPath=os.path.expanduser('~/Nextcloud/Superconducting RF Section/Temperature Sensors')
    print(*vstring())
    print('Data in: ',clr(f'{CERNOXclasses.CXTS.dataPath:s}',color='light_yellow',on_color='on_light_red'))
    print(clr('use the --help argument for a brief help message.',color='green'))

    parser = argparse.ArgumentParser(description='CERNOX files manager.',prog='CERNOXmanage',
                        epilog='This tool assists in the management of the CERNOX datafiles in the MeasurementDB')

    group=parser.add_mutually_exclusive_group()
    group.add_argument('--fic', action='store_true',  help='List .fic files in DB')
    group.add_argument('--hdf', action='store_true',  help='List .hdf files in DB')
    group.add_argument('--cof', action='store_true',  help='List .cof files in DB')
    group.add_argument('--f34', action='store_true',  help='List .340 files in DB')
    group.add_argument('--convert',action='store_true', help='Convert fic files to HDF in DB, if missing')
    group.add_argument('--cheb',action='store_true', help='Add Chebyshev fit to the HDF-converted files in DB, if missing')
    group.add_argument('--c34',action='store_true', help='Convert into .340 the HDF-converted files in DB, if missing')
    group.add_argument('--regenerate',action='store_true', help='Regenerate HDF and COF files for all fic files in DB')
    group.add_argument('--xls', action='store', help='Reads the excel file with the module configuration and copies files in current folder')
    group.add_argument('--TS2', action='store', help='Reads the excel file with the module configuration and puts in the TS2 Namespace')
    group.add_argument('--plot', action='store_true', help='Plot the sensor data and fits')
    group.add_argument('--temp',action='store',help='Print the R at the selected temperature')

    parser.add_argument('--verbosity','-v',action='count',help='Increase output verbosity')
    parser.add_argument('sensor',help='CERNOX sensor',nargs='?',default='all')
    args = parser.parse_args()

    '''--verbosity'''
    if args.verbosity:
        login=str(os.getlogin())
        uname=str(os.uname()[1])
        print(clr(f'Hello {login} on {uname}',color='dark_grey',attrs=['blink','bold']))

    # Lists of files
    sensorlist, nsens, hdflist, nhdf, coflist, ncof, f34list, nf34 = showDBStatus()
    if args.verbosity:
        print(clr(f'sensor: {args.sensor:s}',color='dark_grey',attrs=['bold','blink']))

    #'''--fic'''
    if args.fic:
        if args.sensor in sensorlist:
            print(clr(f'{args.sensor:s}',color='magenta'),' found in DB')
        elif args.sensor == 'all':
            printSensorList(NITEMS, sensorlist)
        else:
            print(f'{args.sensor:s} not found in DB')

    #'''--hdf'''
    if args.hdf:
        if args.sensor in hdflist:
            print(clr(f'{args.sensor:s}',color='magenta'),' found in DB')
        elif args.sensor == 'all':
            printSensorList(NITEMS, hdflist)
        else:
            print(f'{args.sensor:s} not found in DB')

    #'''--cof'''
    if args.cof:
        if args.sensor in coflist:
            print(clr(f'{args.sensor:s}',color='magenta'),' found in DB')
        elif args.sensor == 'all':
            printSensorList(NITEMS, coflist)
        else:
            print(f'{args.sensor:s} not found in DB')
    #'''--f34'''
    if args.f34:
        if args.sensor in f34list:
            print(clr(f'{args.sensor:s}',color='magenta'),' found in DB')
        elif args.sensor == 'all':
            printSensorList(NITEMS, f34list)
    #'''--convert [sensor|all]'''
    if args.convert:
        # files which are in the list but do not have hdf
        missingfiles=list(set(sensorlist)-set(hdflist))
        if args.sensor in hdflist:
            print(f'{args.sensor:s} already converted')
        elif args.sensor in missingfiles:
            c = convertFICAndSave(args.sensor)
            print(f'Converted {c.Name:s}')
            sensorlist, nsens, hdflist, nhdf, coflist, ncof, f34list, nf34 = showDBStatus()
        elif args.sensor == 'all':
            print(f'Found {len(missingfiles):d} sensor files to convert to HDF')
            for i,f in enumerate(missingfiles,start=1):
                try:
                    c = convertFICAndSave(f)
                    print(f'{i:d}: Converted {c.Name:s}')
                except CERNOXclasses.CXTSException as e:
                    details=e.args[0]
                    print(f"{details["sensor"]}:{details["message"]}")
            print('Done')
            sensorlist, nsens, hdflist, nhdf, coflist, ncof, f34list, nf34 = showDBStatus()
        else:
            print(f'{args.sensor:s} not found in DB')               
    #'''--cheb [sensor|all]'''
    if args.cheb:
        # files which have a hdf but not a cof
        missingcof=list(set(hdflist)-set(coflist))
        additional=[]
        for f in hdflist:
            c = loadHDFsensor(f)
            if not c.hasChebyshev():
                additional.append(f)
        missingfiles=list(set(missingcof) | set(additional))
        if args.sensor in missingfiles:
            c = fitandaddChebyshev(args.sensor)
            print(f'Fitted {c.Name:s}')
            sensorlist, nsens, hdflist, nhdf, coflist, ncof, f34list, nf34 = showDBStatus()
        elif args.sensor == 'all' and missingfiles:
            print(f'Found {len(missingfiles):d} HDF files without COF.')
            for i,f in enumerate(missingfiles,start=1):
                c = fitandaddChebyshev(f)
                print(f'{i:d}: Fitted {c.Name:s}.')
            print('Done')
            sensorlist, nsens, hdflist, nhdf, coflist, ncof, f34list, nf34 = showDBStatus()
        elif args.sensor not in missingfiles:
            print(f'{args.sensor:s} already fitted')
        else:
            print(f'{args.sensor:s} not found in DB')            
    #'''--c34 [sensor|all]'''
    if args.c34:
        # files which have a hdf but not a c34
        missingfiles=list(set(hdflist)-set(f34list))
        if args.sensor in f34list:
            print(f'{args.sensor:s} already converted to .340')
        elif args.sensor in missingfiles:
            c = loadHDFsensor(args.sensor)
            c.save340(CERNOXclasses.CXTS.dataPath)
            print(f'Converted {c.Name:s}')
            sensorlist, nsens, hdflist, nhdf, coflist, ncof, f34list, nf34 = showDBStatus()
        elif args.sensor == 'all':
            print(f'Found {len(missingfiles):d} HDF files without 340 format.')
            for i,f in enumerate(missingfiles,start=1):
                c = loadHDFsensor(f)
                c.save340(CERNOXclasses.CXTS.dataPath)
                print(f'{i:d}: Converted {c.Name:s}.')
            print('Done')
            sensorlist, nsens, hdflist, nhdf, coflist, ncof, f34list, nf34 = showDBStatus()
        else:
            print(f'{args.sensor:s} not found in DB')            
    #'''--regenerate [sensor|all]'''
    if args.regenerate:
        if args.sensor in sensorlist:
            c=convertFICAndSave(args.sensor)
            c.fitChebyshev()
            c.storeChebyshev(CERNOXclasses.CXTS.dataPath)
            c.saveChebyshev(CERNOXclasses.CXTS.dataPath)
            print(f'Regenerated {c.Name:s}.')
            sensorlist, nsens, hdflist, nhdf, coflist, ncof, f34list, nf34 = showDBStatus()
        elif args.sensor == 'all':
            print(f'Found {len(sensorlist):d} sensor files to regenerate.')
            for i,f in enumerate(sensorlist,start=1):
                c=convertFICAndSave(f)
                c.fitChebyshev()
                c.storeChebyshev(CERNOXclasses.CXTS.dataPath)
                c.saveChebyshev(CERNOXclasses.CXTS.dataPath)
                print(f'{i:d}: Regenerated {c.Name:s}.')
            print('Done')
            sensorlist, nsens, hdflist, nhdf, coflist, ncof, f34list, nf34 = showDBStatus()
        else:
            print(f'{args.sensor:s} not found in DB')            
    #'''--xls XLS'''
    if args.xls:
        #file=os.path.join(CERNOXclasses.CXTS.dataPath,args.xls)
        file=args.xls
        if args.xls.startswith('CM00'):
            sheet="instrument"
            sname=r'^CX-M-(.*)$'
        else:
            sheet=None
            wb = load_workbook(file, read_only=True)   # open an Excel file and return a workbook
            for sheetname in ['electric test','electrical test','Electrical test' ]:
                if sheetname in wb.sheetnames:
                    sheet=sheetname
                    break
            if sheet:            
                sname=r'^CX-*[\dX][\dX]-(.*)$'

                print(f'Configuration file {file:s}\nFound sheet {sheet:s}.\nFile exists is {str(os.path.isfile(file)):s}')
                if os.path.isfile(file): config=pd.read_excel(file,sheet)
                moduledict={}
                # Avoid iterrows with pandas, use list comprehension
                info = [(str(n),str(s).removesuffix('OK')) for n,s in zip(config.iloc[:,0],config.iloc[:,1])]
                for ceaname,sensid in info:
                    if args.verbosity: print(ceaname,sensid)
                    if ceaname.startswith('CX') and sensid != 'nan':
                        print(f"Adding {ceaname} with sensor {sensid}")
                        devname=re.findall(sname,ceaname)[0]

                        sensfilename='CX_LS_'+sensid
                        moduledict[devname]={'name' : sensfilename,
                                            'id' : sensid,
                                            'senscof' : sensfilename+'.cof',
                                            'sens340' : sensfilename+'.340',
                                            'idcof' : devname+sensid+'.cof',
                                            'id340' : devname+sensid+'.340',
                                            'okcof' : False,
                                            'ok340' : False}
                if args.verbosity: print(moduledict)
                expected=len(moduledict)
                for k,v in moduledict.items():
                    sourcecof=os.path.join(CERNOXclasses.CXTS.dataPath,'COF',v['senscof'])
                    source340=os.path.join(CERNOXclasses.CXTS.dataPath,'340',v['sens340'])
                    if args.verbosity: 
                        print(sourcecof,'->',v['idcof'])
                        print(source340,'->',v['id340'])
                    if os.path.exists(sourcecof):
                        shutil.copy2(sourcecof,v['idcof'])
                        v['okcof']=True
                    else:
                        print(clr(f"{os.path.basename(sourcecof):s}",color='red')," does not exist!")
                    if os.path.exists(source340):
                        shutil.copy2(source340,v['id340'])
                        v['ok340']=True            
                    else:
                        print(clr(f"{os.path.basename(source340):s}",color='red')," does not exist!")
                cmname=args.xls[0:4]
                good=0
                for k,v in moduledict.items():
                    print(f"{v['name']:s},{v['id']:s},{cmname:s}-{k:s},COF=",end='')
                    if v['okcof']:
                        print(clr(f"{v['okcof']}",color='green'),end='')
                    else:
                        print(clr(f"{v['okcof']}",color='red'),end='')
                    print(",340=",end='')
                    if v['ok340']:
                        print(clr(f"{v['ok340']}",color='green'))
                    else:
                        print(clr(f"{v['ok340']}",color='red'))
                    if v['okcof'] and v['ok340']:
                        good=good+1
                if expected == good:
                    print(f'Expected {expected:d} sensors, ',clr('all copied',color='green'),' in the current directory.')
                else:
                    print(f'Expected {expected:d} sensors, ',clr(f'only {good:d}',color='red'),' copied in the current directory.')
            else:
                print(clr('Uhm, cannot find a sensor sheet, please check CEA file',color='red'))

    #'''--TS2 XLS'''
    if args.TS2:
        file=os.path.join(CERNOXclasses.CXTS.dataPath,args.TS2)
        if args.TS2.startswith('CM00'):
            sheet="instrument"
            sname=r'^CX-M-(.*)$'
        else: 
            wb = load_workbook(file, read_only=True)   # open an Excel file and return a workbook
            if 'electric test' in wb.sheetnames:
                sheet='electric test'
            elif 'elextrical test' in wb.sheetnames:
                sheet="electrical test"
            sname=r'^CX-\d\d-(.*)$'
        print(f'Configuration file {file:s}\nFound sheet {sheet:s}.\nFile exists is {str(os.path.isfile(file)):s}')
        if os.path.isfile(file): config=pd.read_excel(file,sheet)
        moduledict={}
        for index,row in config.iterrows():
            if args.verbosity: print(str(row[0]),str(row[1]))
            ceaname=str(row[0])
            sensid=str(row[1])
            if ceaname.startswith('CX') and sensid != 'nan':
                devname=re.findall(sname,ceaname)[0]
                essdev=f'TE-{int(devname[2:]):03d}'
                sensfilename=f'CX_LS_{sensid}.hdf'
                moduledict[devname]={'sens' : sensfilename,
                                     'id' : f'TS2-010CRM:Cryo-{essdev:s}'}
        if args.verbosity: print(moduledict)
        for k,v in moduledict.items():
            source=os.path.join(CERNOXclasses.CXTS.dataPath,'HDF',v['sens'])
            c=CERNOXclasses.CXTS.LoadHDFfile(source)
            if args.verbosity: print(source,'->',v['id'])
            c.allocateTS2(v['id'],path=CERNOXclasses.CXTS.dataPath)
            
        print(f'Tagged {len(moduledict):d} sensors in the excel file in the TS2 namespace')
    #'''--plot SENSOR'''
    if args.plot:
        if args.sensor in sensorlist:
            name=os.path.join(CERNOXclasses.CXTS.dataPath,'HDF',args.sensor+'.hdf')
            c=CERNOXclasses.CXTS.LoadHDFfile(name)
            # Plot of data and fits
            plt.figure(1)
            ax=plt.gca()
            plt.xlabel('R Measured [Ohm]')
            plt.ylabel('T measured [K]')
            plt.title(str(c))
            x=c.Data['Rmes(Ohm)']
            y=c.Data['Tmes(K)']
            plt.loglog(x,y,'ob',label='Measurements',markersize=2)
            yfit=c.IPNOEval(x)
            plt.loglog(x,yfit,'r-',label='IPNO Fit',linewidth=1)
            plt.grid()
            plt.legend()
            plt.show()
            # Cheby
            if c.hasChebyshev():
                plt.figure(2)
                ax=plt.gca()
                plt.xlabel('R Measured [Ohm]')
                plt.ylabel('T measured [K]')
                plt.title(str(c))
                x=np.array(c.Data['Rmes(Ohm)'])
                y=np.array(c.Data['Tmes(K)'])
                plt.loglog(x,y,'ob',label='Measurements',markersize=2)
                yfit=c.evalChebyshev(x)
                plt.loglog(x,yfit,'g-',label='Chebyshev Fit',linewidth=1)
                c1fmt="Z=({:.2f},{:.2f}) C={:f},{:f},{:f},{:f},{:f},{:f},{:f},{:f},{:f}"
                cdict=c._cheby["Range1"]
                c1str=c1fmt.format(cdict['ZL'],cdict['ZU'],*cdict['coeff'])
                plt.text(0.01,0.20,'Chebyshev Polynomials',ha='left',va='center',
                        transform=ax.transAxes,size=6)
                plt.text(0.01, 0.15, c1str, ha='left',va='center', 
                        transform=ax.transAxes,size=6)
                c2fmt="Z=({:.2f},{:.2f}) C={:f},{:f},{:f},{:f},{:f},{:f},{:f},{:f}"
                cdict=c._cheby["Range2"]
                c2str=c2fmt.format(cdict['ZL'],cdict['ZU'],*cdict['coeff'])
                plt.text(0.01, 0.10, c2str, ha='left',va='center', 
                        transform=ax.transAxes,size=6)
                cdict=c._cheby["Range3"]
                c3str=c2fmt.format(cdict['ZL'],cdict['ZU'],*cdict['coeff'])
                plt.text(0.01, 0.05, c3str, ha='left',va='center', 
                        transform=ax.transAxes,size=6)
                plt.grid()
                plt.legend()
                plt.show()
        elif args.sensor == 'all':
            print('Cannot plot all sensors... Choose one!')
        else:
            print(f'{args.sensor:s} not in list of sensors converted to HDF file!')
    #'''--temp TEMP'''
    if args.temp:
        if args.sensor in sensorlist:
            name=os.path.join(CERNOXclasses.CXTS.dataPath,'HDF',args.sensor+'.hdf')
            c=CERNOXclasses.CXTS.LoadHDFfile(name)
            # Cheby
            if c.hasChebyshev():
                R=np.array(c.Data['Rmes(Ohm)'])
                T=np.array(c.Data['Tmes(K)'])
                tval=float(args.temp)
                if tval<np.min(T) or tval>np.max(T):
                    print(f'TEMP={tval:.2f} out of range')
                else:
                    #
                    for r,t in zip(R,T):
                        if t<tval:
                            tapp=t
                            rapp=r
                            break
                    print(f'At t={tapp:.2f} R={rapp:.2f}')
                    #yfit=c.evalChebyshev(x)
            else:
                print(f'Please update Chebyshev fit of sensor {c.Name:s}')
        elif args.sensor == 'all':
            print('Cannot give you all sensors... Choose one!')
        else:
            print(f'{args.sensor:s} not in list of sensors converted to HDF file!')
