# Paolo
import re 
import os
import glob
import h5py
import numpy as np
import pandas as pd

DEBUG=False

# TODO: For later handle exceptions in a class.
class CXTSException(Exception):
    '''Future: implement exceptions'''
    pass

# Main CERNOX Class, use a simple name, it allows access to a temperature sensor, 
# its calibration data or measurements
# The senso can be created ONLY if it has the two files for the two ranges!!!
class CXTS(object):
    """ CERNOX calibration class to handle cernox calibration"""
    ## class variable shared by all instances
    dataPath=None
    ## CONSTRUCTORS
    def __init__(self):
        """ empty constructor """
        ## super() function allows the access to original method of a mother class
        super().__init__()
        ## private variables initialization
        self._name=None
        self._environment=None
        self._date=None
        ## Dictionary containing the 1D array of calibration coefficients dataset
        self._coeffdataset={}
        self._coeffdataset['Range1']=None
        self._coeffdataset['Range2']=None
        ## Dictionary of data structures
        self._data={}
        self._data['Range1']=None
        self._data['Range2']=None
        ## Dictionary of chebyshev coefficients
        self._cheby={}

    ################################ CLASS METHODS
    @classmethod
    def _IPNOfiles(cls,sensorname: str):
        ''' Searches the two fic files,  probe existence, and returns a list with them'''
        searchpath=os.path.join(CXTS.dataPath,'FIC',sensorname+'(1.*.fic')
        file1=glob.glob(searchpath)
        if len(file1) !=1:
            raise CXTSException({"message":'Wrong number of files for Range 1',"sensor":sensorname})
        searchpath=os.path.join(CXTS.dataPath,'FIC',sensorname+'(10*.fic')
        file2=glob.glob(searchpath)
        if len(file2) !=1:
            raise CXTSException({"message":'Wrong number of files for Range 2',"sensors":sensorname})
        return [file1[0],file2[0]]

    #Decorators are a syntactic convenience, that allows a Python source file to say what it is going to do with the result of a function or a class statement before rather than after the statement
    @classmethod
    def Initialize(cls,sensorname):
        """ Initialize with explicit arguments (constructor)"""
        # Retrieves the files, raising exceptions if not ok
        files=CXTS._IPNOfiles(sensorname)
        if not files:
            raise CXTSException({"message":'Cannot find sensor files',"sensor": sensorname})
        h_1,c_1,d_1=CXTS._loadFICinfo(files[0])
        h_2,c_2,d_2=CXTS._loadFICinfo(files[1])
        # Create a new class instance
        c=cls()
        # Checks if the two files refer to the same sensor.
        # The headers should match, otherwise an exeption is raised.
        for k in h_1.keys():
            if h_1[k] != h_2[k]:
                raise CXTSException({"message":'ouch, info in the two files is different!',"sensor": sensorname})
        # Then just uses data from one file to populate the properties
        c._name=h_1['Thermometre']
        c._environment=h_1['Environment']
        c._date=h_1['Date']
        # Dataset of calibration coefficients
        c._coeffdataset['Range1']=c_1[1]
        c._coeffdataset['Range2']=c_2[1]
        # Original data points from IPNO
        c._data['Range1']=d_1
        c._data['Range2']=d_2
        return c

    @classmethod
    def _loadFICinfo(cls, filename):
        ''' Loads a FIC file from IPNO calibration'''
        # first four rows
        headers=pd.read_csv(filename,nrows=4,header=None,delimiter=':',na_values='END')
        # remove the annoying tab character, stripping all elements
        headers=headers.map(lambda x: x.strip())
        # creates a dictionary
        headers_dict=dict(headers.values)
        # fifth row is only text
        # then 10 rows of coefficients, read and dictionary
        coefficients=pd.read_csv(filename,skiprows=5,nrows=10,header=None,delimiter='=',na_values='END')
        # now the data
        data=pd.read_csv(filename,skiprows=15,header=0,delimiter='\t',na_values='END')
        sorted_data=data.sort_values('Rmes(Ohm)',ascending=True)
        return headers_dict,coefficients,sorted_data

    @classmethod
    def LoadHDFfile(cls,file):
        ''' Load from HDF file '''
        try:
            c=CXTS()            
            infile=h5py.File(file,'r')
            c._name=infile.attrs["SerialNo"]
            grp=infile["measurementdata"]
            c._environment=grp.attrs["environment"]
            c._date=grp.attrs["date"]
            grp=infile["measurementdata/Range1"]
            c._coeffdataset['Range1']=pd.Series(grp["Coefficients"])
            c._data['Range1']=pd.DataFrame({"Rmes(Ohm)" : grp["Data"][:,0],
                                           "Tmes(K)" : grp["Data"][:,1],
                                           "Tfit(K)" : grp["Data"][:,2],
                                           "dT(mK)" : grp["Data"][:,3],
                                           "Courant(A)": grp["Data"][:,4]})
            grp=infile["measurementdata/Range2"]
            c._coeffdataset['Range2']=pd.Series(grp["Coefficients"])
            c._data['Range2']=pd.DataFrame({"Rmes(Ohm)" : grp["Data"][:,0],
                                           "Tmes(K)" : grp["Data"][:,1],
                                           "Tfit(K)" : grp["Data"][:,2],
                                           "dT(mK)" : grp["Data"][:,3],
                                           "Courant(A)": grp["Data"][:,4]})
            if 'calibrationdata' in infile.keys():
                grp=infile["calibrationdata"]
                dset=grp['Coeff1']
                c._cheby['Range1']={'coeff' : np.array(dset), 
                                    'RL' : dset.attrs['RL'], 'RU' : dset.attrs['RU'],
                                    'ZL' : dset.attrs['ZL'], 'ZU' : dset.attrs['ZU']}
                dset=grp['Coeff2']
                c._cheby['Range2']={'coeff' : np.array(dset), 
                                    'RL' : dset.attrs['RL'], 'RU' : dset.attrs['RU'],
                                    'ZL' : dset.attrs['ZL'], 'ZU' : dset.attrs['ZU']}
                dset=grp['Coeff3']
                c._cheby['Range3']={'coeff' : np.array(dset), 
                                    'RL' : dset.attrs['RL'], 'RU' : dset.attrs['RU'],
                                    'ZL' : dset.attrs['ZL'], 'ZU' : dset.attrs['ZU']}
            infile.close()
            return c
        except IOError as err:
            print(f'Error: {err}')
            raise
        except ValueError as err:
            print(err.args)
            raise

    @classmethod 
    def getSensorList(cls) -> list:
        ''' Returns a list with the the sensors CX_LS_*.fic files'''
        filelist=os.listdir(os.path.join(CXTS.dataPath,'FIC'))
        sensorlist=[]
        patt=re.compile(r'^(?:ESS-\d{7} - )*(CX_LS_.*)\(.*\)\.fic$')
        for f in filelist:
            m=patt.match(f)
            if m:
                sensorlist.append(m.group(1))
        return list(dict.fromkeys(sensorlist))
    @classmethod
    def getCOFList(cls) -> list:
        ''' Returns a list with the sensors CX_LS_*.cof files'''
        filelist=os.listdir(os.path.join(CXTS.dataPath,'COF'))
        coflist=[]
        patt=re.compile(r'^(CX_LS_.*)\.cof$')
        for f in filelist:
            m=patt.match(f)
            if m:
                coflist.append(m.group(1))
        return list(dict.fromkeys(coflist))
    @classmethod
    def getHDFList(cls):
        ''' Returns a list with the sensors CX_LS_*.hdf files'''
        filelist=os.listdir(os.path.join(CXTS.dataPath,'HDF'))
        hdflist=[]
        patt=re.compile(r'^(CX_LS_.*)\.hdf$')
        for f in filelist:
            m=patt.match(f)
            if m:
                hdflist.append(m.group(1))
        return list(dict.fromkeys(hdflist))
    @classmethod
    def get340List(cls):
        '''Returns a list with the sensors CX_LS_*.340 files'''
        filelist=os.listdir(os.path.join(CXTS.dataPath,'340'))
        f34list=[]
        patt=re.compile(r'^(CX_LS_.*)\.340$')
        for f in filelist:
            m=patt.match(f)
            if m:
                f34list.append(m.group(1))
        return list(dict.fromkeys(f34list))

    ################################ PROPERTIES
    # Properties for readability
    @property
    def Name(self):
        '''Name of the sensor'''
        return self._name
    @property
    def Environment(self):
        '''The calibration facility'''
        return self._environment
    @property
    def Date(self):
        '''The calibration date'''
        return self._date
    @property
    def IPNOCalData(self):
        '''The IPNO default calibration set, in two ranges'''
        return self._coeffdataset
    @property
    def DataRange(self):
        '''The data ranges, as DataFrames stored in a dictionary'''
        return self._data
    @property
    def Data(self):
        '''The full DataFrame of measured data'''
        ranges=[self._data['Range2'],self._data['Range1']]
        return pd.concat(ranges,ignore_index=True,sort=False)
    @property
    def Chebyshev(self):
        '''The dictionary of Chebyshev parameters'''
        return self._cheby

    ################################ GENERIC OVVERRIDE METHODS
    # definition of str to be able to 'print' what  want to check
    def __str__(self):
        return 'I am: {:s}, measured on {:s} at {:s}.'.format(self._name,self._date,self._environment)

    ################################ INSTANCE METHODS
    def IPNOEval(self,x):
        '''Function to return the IPNO evaluated function'''
        # This function works both on single values or array-like
        # structures, transparently.
        R2rmin=np.min(self._data['Range1']['Rmes(Ohm)'])
        # If we get a single value we transform it into an length 1 array
        if isinstance(x,float):
            xx=np.array([x])
        else:
            xx=x
        y=np.array([])
        for xv in xx:
            if xv>R2rmin:
                a=self._coeffdataset['Range1']
            else:
                a=self._coeffdataset['Range2']
            avect=np.array(a)
            logvect=np.array([np.log10(xv)**i for i in range(0,10)])
            lyfit=sum(avect/logvect)
            y=np.append(y,10**lyfit)
        # If we got a single value we just output the single value
        if type(x) is float:
            return y[0]
        else:
            return y

    def HDFName(self):
        return f'{self._name:s}.hdf'
    def COFName(self):
        return f'{self._name:s}.cof'
    def f34Name(self):
        return f'{self._name:s}.340'

    # Implement JAP Structure for Nexus HDF
    def WriteHDFFile(self,path=None,mode='w'):
        ''' Write an HDF file, according to the structure defined in the PPT '''
        try:
            fname=self.HDFName()
            if path == None:
                outname=fname
            else:
                outname=path+'/HDF/'+fname
            if mode not in ['w', 'a']:
                return outname
            outfile=h5py.File(outname,mode)
            # file attributes
            outfile.title                       = 'CERNOX '+self._name
            outfile.attrs["SerialNo"]           = self._name
            outfile.attrs["FileName"]           = outname
            outfile.attrs["Calibration Date"]   = self._date
            outfile.attrs["AssetId"]            = ""
            outfile.attrs["Creator"]            = "CERNOXmanage.py"
            outfile.attrs["NeXus_Version"]      = "4.3.0"
            outfile.attrs["HDF5_Version"]       = h5py.version.hdf5_version
            outfile.attrs["H5py_Version"]       = h5py.version.version
            outfile.attrs["EssName"]            = ''
            outfile.attrs["TS2Name"]            = ''
            # measurementdata group
            grp=outfile.create_group('measurementdata')
            grp.attrs["NX_class"]       = 'NXentry'
            grp.attrs["environment"]    = self._environment
            grp.attrs["date"]           = self._date
            r1=grp.create_group('Range1')
            r1.attrs["NX_class"]        = 'NXdata'
            ds = r1.create_dataset('Coefficients',data=self._coeffdataset['Range1'])
            ds = r1.create_dataset('Data',data=self._data['Range1'])
            r2=grp.create_group('Range2')
            r2.attrs["NX_class"]        = 'NXdata'
            r2.create_dataset('Coefficients',data=self._coeffdataset['Range2'])
            r2.create_dataset('Data',data=self._data['Range2'])
            # user group
            grp = outfile.create_group('userdata')
            grp.attrs['NX_class']       = 'NXuser'
            grp.attrs['name']           = os.getlogin()
            grp.attrs['role']           = 'HDF creator'
            grp.attrs['affiliation']    = 'ESS'
            #
            if self._cheby:
                grp=outfile.create_group('calibrationdata')
                grp.attrs["NX_class"]           = 'NXdata'
                grp.attrs["CalibrationType"]    = 'Chebyshev'
                ds1=grp.create_dataset("Coeff1",data=self._cheby['Range1']['coeff'])
                ds1.attrs['RL'] = self._cheby['Range1']['RL']
                ds1.attrs['RU'] = self._cheby['Range1']['RU']
                ds1.attrs['ZL'] = self._cheby['Range1']['ZL']
                ds1.attrs['ZU'] = self._cheby['Range1']['ZU']
                ds2=grp.create_dataset("Coeff2",data=self._cheby['Range2']['coeff'])
                ds2.attrs['RL'] = self._cheby['Range2']['RL']
                ds2.attrs['RU'] = self._cheby['Range2']['RU']
                ds2.attrs['ZL'] = self._cheby['Range2']['ZL']
                ds2.attrs['ZU'] = self._cheby['Range2']['ZU']
                ds3=grp.create_dataset("Coeff3",data=self._cheby['Range3']['coeff'])
                ds3.attrs['RL'] = self._cheby['Range3']['RL']
                ds3.attrs['RU'] = self._cheby['Range3']['RU']
                ds3.attrs['ZL'] = self._cheby['Range3']['ZL']
                ds3.attrs['ZU'] = self._cheby['Range3']['ZU']
            # close
            outfile.close()
            return outname
        except IOError as err:
            print(f'Error: {err}')
            raise
        except OSError as err:
            print(f'OS Error: {err}')
            raise
        except ValueError as err:
            print(err.args)
            raise        

    def fitChebyshev(self):
        ''' Finds the  Chebyshev fit of the data, in three standard ranges
            Range 3: 80.2-325 K, 8 coefficients
            Range 2: 14.2-80.2 K, 8 coefficients
            Range 1: 1.4-14.2 K, 9 coefficients
        '''        
        T=np.array(self.Data['Tmes(K)'])
        R=np.array(self.Data['Rmes(Ohm)'])
        #Range 3: 80.2-325 K, 6-8 coefficients typ
        I3=(T<325) & (T>=80.2)
        T3=T[I3]
        R3=R[I3]
        ZL=np.log10(np.min(R3))
        ZU=np.log10(np.max(R3))
        Z3=np.log10(R3)
        K3=((Z3-ZL)-(ZU-Z3))/(ZU-ZL)
        coef3=np.polynomial.chebyshev.chebfit(K3,T3,7)
        self._cheby["Range3"]={"coeff" : coef3, 
                                "RL" : np.min(R3), "RU"  : np.max(R3),
                                "ZL" : ZL, "ZU" : ZU}
        #Range 2: 14.2-80.2 K, 6-8 coefficients typ
        #Remind the lower temperature of T3 to use as upper for T2
        I2=(T<np.min(T3)) & (T>=14.2)
        T2=T[I2]
        R2=R[I2]
        #And use the previous ZU
        ZL=ZU
        #ZL=np.log10(np.min(R2))
        ZU=np.log10(np.max(R2))
        Z2=np.log10(R2)
        K2=((Z2-ZL)-(ZU-Z2))/(ZU-ZL)
        coef2=np.polynomial.chebyshev.chebfit(K2,T2,7)
        #And previous max R3
        self._cheby["Range2"]={"coeff" : coef2, 
                                "RL" : np.max(R3), "RU"  : np.max(R2),
                                "ZL" : ZL, "ZU" : ZU}
        #Range 1: 1.4-14.2, 8-9 coefficients typ
        #Remember the lower temperaturs of T2 to use as upper for T1
        I1=(T<np.min(T2)) & (T>=1.4)
        T1=T[I1]
        R1=R[I1]
        #And use the previous ZU
        ZL=ZU
        #ZL=np.log10(np.min(R1))
        ZU=np.log10(np.max(R1)+1)
        Z1=np.log10(R1)
        K1=((Z1-ZL)-(ZU-Z1))/(ZU-ZL)
        coef1=np.polynomial.chebyshev.chebfit(K1,T1,8)
        #And previous max R2
        self._cheby["Range1"]={"coeff" : coef1, 
                                "RL" : np.max(R2), "RU"  : np.max(R1)+1,
                                "ZL" : ZL, "ZU" : ZU}

    def storeChebyshev(self,path=None):
        ''' Store  the Chebyshev fit into the HDF file'''
        if self._cheby:
            fname=self.HDFName()
            if path is None:
                outname=fname
            else:
                outname=path+'/HDF/'+fname
            try:
                outfile=h5py.File(outname,'r+')
                if 'calibrationdata' in outfile.keys():
                    del outfile['calibrationdata']
                grp=outfile.create_group('calibrationdata')
                grp.attrs["NX_class"]           = 'NXdata'
                grp.attrs["CalibrationType"]    = 'Chebyshev'
                ds1=grp.create_dataset("Coeff1",data=self._cheby['Range1']['coeff'])
                ds1.attrs['RL'] = self._cheby['Range1']['RL']
                ds1.attrs['RU'] = self._cheby['Range1']['RU']
                ds1.attrs['ZL'] = self._cheby['Range1']['ZL']
                ds1.attrs['ZU'] = self._cheby['Range1']['ZU']
                ds2=grp.create_dataset("Coeff2",data=self._cheby['Range2']['coeff'])
                ds2.attrs['RL'] = self._cheby['Range2']['RL']
                ds2.attrs['RU'] = self._cheby['Range2']['RU']
                ds2.attrs['ZL'] = self._cheby['Range2']['ZL']
                ds2.attrs['ZU'] = self._cheby['Range2']['ZU']
                ds3=grp.create_dataset("Coeff3",data=self._cheby['Range3']['coeff'])
                ds3.attrs['RL'] = self._cheby['Range3']['RL']
                ds3.attrs['RU'] = self._cheby['Range3']['RU']
                ds3.attrs['ZL'] = self._cheby['Range3']['ZL']
                ds3.attrs['ZU'] = self._cheby['Range3']['ZU']
                outfile.close()
            except:
                print("Problem accessing file")
        else:
            print("No Chebyshev fit exist!")   

    def hasChebyshev(self):
        if self._cheby:
            return True
        else:
            return False

    def allocateTS2(self,ts2tag,path=None):
            fname=self.HDFName()
            if path is None:
                outname=fname
            else:
                outname=path+'/HDF/'+fname
            outfile=h5py.File(outname,'r+')
            outfile.attrs['TS2Name'] = ts2tag
            outfile.close()

    def evalChebyshev(self,x):
        ''' Evaluate the Chebyshev fit on a float or array x'''
        if self._cheby:
            if isinstance(x,float):
                xx=np.array([x])
            else:
                xx=x
            y=np.array([])
            zz=np.log10(xx)
            for x,z in zip(xx,zz):
                found=False
                for key,v in self._cheby.items():
                    zl=v['ZL']
                    zu=v['ZU']
                    rl=v['RL']
                    ru=v['RU']
                    coeff=v['coeff']
                    if (x>=rl) & (x<ru):
                        k=((z-zl)-(zu-z))/(zu-zl)
                        t=0.0
                        for i in range(0,len(coeff)):
                            t=t+coeff[i]*np.cos(i*np.arccos(k))
                        y=np.append(y,t)
                        found=True
                        break
                if not found: raise CXTSException({"message":f"{x:f} does not fit in range!","sensor":self._name})
            if type(x) is float:
                return y[0]
            else:
                return y
        else:
            print("No Chebyshev fit exist!")   

    def saveChebyshev(self,path=None):
        ''' Save the COF file'''
        if self._cheby:
            fname=self.COFName()
            if path == None:
                outname=fname
            else:
                outname=os.path.join(path,'COF',fname)
            pass
            of=open(outname,'w',encoding='utf8')
            c=self._cheby
            of.write('Number of fit ranges: {:d}\n'.format(len(c)))
            for k in sorted(c):
                i=int(re.findall(r'Range(\d)',k)[0])
                of.write('FIT RANGE: {:d}\n'.format(i))
                of.write('Fit type for range{:d}: LOG\n'.format(i))
                of.write('Order of fit range{:d}: {:d}\n'.format(i,len(c[k]['coeff'])-1))
                of.write('Zlower for fit range{:d}: {:.14f}\n'.format(i,float(c[k]['ZL'])))
                of.write('Zupper for fit range{:d}: {:.14f}\n'.format(i,float(c[k]['ZU'])))
                of.write('Lower Resist. limit for fit range{:d}: {:.4f}\n'.format(i,float(c[k]['RL'])))
                of.write('Upper Resist. limit for fit range{:d}: {:.4f}\n'.format(i,float(c[k]['RU'])))
                coeffs=np.array(c[k]['coeff'])
                for j in range(0,len(coeffs)):
                    of.write('C({:d}) Equation {:d}: {:.14e}\n'.format(j,i,coeffs[j]))
            of.close()
        else:
            print("No Chebyshev fit exist!")

    def save340(self,path=None):
        fname=self.f34Name()
        if path is None:
            outname=fname
        else:
            outname=os.path.join(path,'340',fname)
        df=self.Data.sort_values(by='Tmes(K)', ascending=False)
        tmax=df['Tmes(K)'].max()
        of=open(outname,'w',encoding='utf8')
        of.write('Sensor Model:   CX-1050-CU-HT-1.4L\r\n')
        of.write(f'Serial Number: {self.Name:s}\r\n')
        of.write('Data Format:    4      (Log Ohms/Kelvin)\r\n')
        of.write(f'SetPoint Limit: {tmax:7.3f}     (Kelvin)\r\n')
        of.write('Temperature coefficient:  1 (Negative)\r\n')
        numpoint=len(list(df.iterrows()))
        of.write(f'Number of Breakpoints:   {numpoint:d}\r\n\r\n')
        of.write('No.   Units      Temperature (K)\r\n\r\n')
        for i,r in df.iterrows():
            of.write(f'{i+1:3d}  {np.log10(r['Rmes(Ohm)']):7.5f}       {r['Tmes(K)']:7.3f}\r\n')
        of.close()

## self testing code
if __name__ == '__main__':
    CXTS.dataPath=os.path.expanduser('~/MeasurementDB/Accelerator Division/SRF Components/Temperature Sensors')
    print("h5py=",h5py.version.version)
    print("hdf5=",h5py.version.hdf5_version)
    if False:
        c=CXTS.Initialize('CX_LS_X107512')
        print(c)
        print(c.Name)
        print(c.Environment)
        print(c.Date)
        print(c.IPNOCalData['Range1'])
        print(type(c.IPNOCalData['Range1']))
        print(c.IPNOCalData['Range1'][1])
        print(c.DataRange['Range1'][0:5])
        print(type(c.DataRange['Range1']))
        print(c.DataRange['Range1']['Tmes(K)'][0:5])
        print(c.Data[0:5])
        print(c.Data[-5:])
        c.WriteHDFFile(CXTS.dataPath)
        c.fitChebyshev()
        c.storeChebyshev(CXTS.dataPath)
        print(c._cheby['Range1'])
        name=os.path.join(CXTS.dataPath,'HDF','CX_LS_X107512.hdf')
        cc=CXTS.LoadHDFfile(name)
        print(cc)
        print(cc.Name)
        print(cc.Environment)
        print(cc.Date)
        print(cc.IPNOCalData['Range1'])
        print(type(cc.IPNOCalData['Range1']))
        print(cc.IPNOCalData['Range1'][1])
        print(cc.DataRange['Range1'][0:5])
        print(type(cc.DataRange['Range1']))
        print(cc.DataRange['Range1']['Tmes(K)'][0:5])
        print(cc.Data[0:5])
        print(cc.Data[-5:])
        print(cc._cheby['Range1'])

        print(c.evalChebyshev(60.0))
        print(c.evalChebyshev(7000.0))
        c.saveChebyshev(CXTS.dataPath)
