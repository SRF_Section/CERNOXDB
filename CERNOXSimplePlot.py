import matplotlib.pyplot as plt
import numpy as np
import CERNOXclasses
import os

if __name__ == '__main__':
    CERNOXclasses.CXTS.dataPath=os.path.expanduser('~/MeasurementDB/Accelerator Division/SRF Components/Temperature Sensors')
    # We can initialize from fic
    #c=CERNOXclasses.CXTS.Initialize('CX_LS_X107512')
    # Or we can directly read the HDF
    c=CERNOXclasses.CXTS.LoadHDFfile('CX_LS_X107512.hdf')
    # Plot of data and fits
    plt.figure(1)
    ax=plt.gca()
    plt.xlabel('R Measured (Ohm)')
    plt.ylabel('T measured (K)')
    plt.title(str(c))
    coeff=c.IPNOCalData
    x1=c.DataRange['Range1']['Rmes(Ohm)']
    y1=c.DataRange['Range1']['Tmes(K)']
    plt.loglog(x1,y1,'ob',label='Range1',markersize=2)
    yfit1=c.IPNOEval(x1)
    plt.loglog(x1,yfit1,'b-',linewidth=1)
    x2=c.DataRange['Range2']['Rmes(Ohm)']
    y2=c.DataRange['Range2']['Tmes(K)']
    plt.loglog(x2,y2,'or',label='Range2',markersize=2)
    plt.grid()
    yfit2=c.IPNOEval(x2)
    plt.loglog(x2,yfit2,'r-',linewidth=1)
    plt.legend()
    plt.show()
    # Difference fit vs data
    plt.figure(2)
    ax=plt.gca()
    plt.xlabel('R Measured (Ohm)')
    plt.ylabel('Delta T (mK)')
    plt.title(str(c))
    plt.plot(x1,(y1-yfit1)*1000,'ob',label='Range1',markersize=2)
    plt.plot(x2,(y2-yfit2)*1000,'or',label='Range2',markersize=2)
    plt.legend()
    plt.show()
    # retrieve data in single range
    plt.figure(3)
    ax=plt.gca()
    plt.xlabel('R Measured (Ohm)')
    plt.ylabel('T measured (K)')
    plt.title(str(c))
    x=c.Data['Rmes(Ohm)']
    y=c.Data['Tmes(K)']
    plt.loglog(x,y,'ob',label='Measurements',markersize=2)
    yfit=c.IPNOEval(x)
    plt.loglog(x,yfit,'r-',label='IPNO Fit',linewidth=1)
    plt.grid()
    plt.legend()
    plt.show()
    #print(c.IPNOEval(200.))
    # Cheby
    plt.figure(4)
    ax=plt.gca()
    plt.xlabel('R Measured (Ohm)')
    plt.ylabel('T measured (K)')
    plt.title(str(c))
    x=np.array(c.Data['Rmes(Ohm)'])
    y=np.array(c.Data['Tmes(K)'])
    plt.loglog(x,y,'ob',label='Measurements',markersize=2)
    yfit=c.evalChebyshev(x)
    plt.loglog(x,yfit,'g-',label='Chebyshev Fit',linewidth=1)
    c1fmt="Z=({:.2f},{:.2f}) C={:f},{:f},{:f},{:f},{:f},{:f},{:f},{:f},{:f}"
    cdict=c._cheby["Range1"]
    c1str=c1fmt.format(cdict['ZL'],cdict['ZU'],*cdict['coeff'])
    plt.text(0.01,0.20,'Chebyshev Polynomials',ha='left',va='center',
             transform=ax.transAxes,size=6)
    plt.text(0.01, 0.15, c1str, ha='left',va='center', 
             transform=ax.transAxes,size=6)
    c2fmt="Z=({:.2f},{:.2f}) C={:f},{:f},{:f},{:f},{:f},{:f},{:f},{:f}"
    cdict=c._cheby["Range2"]
    c2str=c2fmt.format(cdict['ZL'],cdict['ZU'],*cdict['coeff'])
    plt.text(0.01, 0.10, c2str, ha='left',va='center', 
              transform=ax.transAxes,size=6)
    cdict=c._cheby["Range3"]
    c3str=c2fmt.format(cdict['ZL'],cdict['ZU'],*cdict['coeff'])
    plt.text(0.01, 0.05, c3str, ha='left',va='center', 
              transform=ax.transAxes,size=6)
    plt.grid()
    plt.legend()
    plt.show()